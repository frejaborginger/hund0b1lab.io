---
layout: page
title: My battlestation
permalink: /battlestation.html
---

<div style="margin: -10px 0 20px 0; color: #828282; font-size: 12px">Atlas</div>

[![](/images/thumbnails/my_battlestation_january_2018.jpg)](/images/my_battlestation_january_2018.jpg)

| Processor:    | Intel Core i5-2500K, 3,3GHz |
| Motherboard:    | ASUS P8P67 PRO B3 |
| Memory:        | Corsair Vengeance DDR3 1600MHz, 12GB |
| Graphics card:   | Gigabyte GTX780 |
| Solid state drive:          | Intel 320 Series, 120GB |
| Power supply:  | Corsair RM750 |
| Case:       | Fractal Design Define R3 |
| Monitors: | Samsung S24C750P |
| Mouse: | SteelSeries Xai |
| Mouse mat: | SteelSeries 4HD |
| Desktop: | Some old one from IKEA, it's getting replaced soon™. |

# Other computers

## HTPC/VPS-host

<div style="margin: -5px 0 20px 0; color: #828282; font-size: 12px">Fjuppen</div>

| Processor:    | AMD Ryzen 5 2400G, 3,6GHz |
| Motherboard:    | Asrock A320M-DGS |
| Memory:        | Kingston ValueRam DDR4 2133MHz ECC CL15, 8GB |
| Graphics card:   | X |
| Solid state drive:          | Samsung 850 Pro Series, 128GB |
| Hard drives:   | WD Red, 2TB |
|               | Seagate IronWolf, 4TB |
| Power supply:  | Corsair HX620 |
| Case:       | Fractal Design Define Mini C |
