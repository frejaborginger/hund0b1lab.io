---
layout: post
title: StreamCurse - A Ncurses frontend for Streamlink
tags:
 - StreamCurse
 - Streamlink
 - Twitch
 - Ncurses
 - Applications
---

When I'm at the computer I often like to have some background noise to keep me company at nights. [Twitch](https://twitch.tv) and other livestreaming services is perfect for that, you really don't get distracted and you don't miss anything if you don't pay attention to it either.

For this I like to use the application [StreamCurse](https://github.com/RadicalEd360/StreamCurse), which is a Ncurses frontend to [Streamlink](https://github.com/streamlink/streamlink), a CLI-tool that lets you extract livestreams from [various services](https://streamlink.github.io/plugin_matrix.html) and play them via your media player of choice, mine happens to be [mpv](https://mpv.io).

![](/images/streamcurse.png)

What StreamCurse does is that it lets you collect all your favourite livestreams from various services in one place and it will then keep you updated about the livestreams that's currently broadcasting. The nice thing with this solution is that I can fully avoid bloated and/or sites that tracks you, like Twitch and YouTube. I also don't need an account to follow anyone and don't even have to leave the safety of my own desktop. :)

The interface is very straight forward, it uses a Vi-like navigation with very few and simple keybindings, I have listed some of the more common ones down below:

| a | Add stream |
| d | Delete stream |
| Enter | Start stream |
| r | Change stream resolution |
| n | Change stream name |
| u | Change stream URL |
| ? | List all shortcuts |

You can set a default resolution for all new streams, but it's also possible to change the resolution per stream with the key `r`. This is something I currently have to do for livestreams from YouTube due to the streams at 1080p/60FPS is using [DASH](https://en.wikipedia.org/wiki/Dynamic_Adaptive_Streaming_over_HTTP), which is [not yet supported](https://github.com/streamlink/streamlink/pull/225#issuecomment-263821938) and as a workaround I have limited the resolution to 1080p/30FPS for those streams.

# Installation

The package `streamlink` is available in the repositories for both Gentoo Linux and Arch Linux. It's as far as I know not available in Ubuntu, but since it's just a regular Python application you can install it via `pip`:

```
pip install streamlink --user
```

To install `streamcurse` you need to download the source code from the git repository and manually install it yourself:

```
$ git clone https://github.com/RadicalEd360/streamcurse
$ cd streamcurse
# python3 setup.py install
```

# Configuration

You can start adding streams right away, but if you do have a Twitch-account you can choose to sync all the channels you follow with the keybinding `w` followed by your username.

The configuration can be found in the folder `$CONFIG/streamcurse`, if you want to change any settings like the colors or permanently adding your Twitch-username.
