---
layout: post
title: Abook - A Ncurses address book application
tags:
  - Abook
  - Adress_book
  - Applications
  - Mutt
  - NeoMutt
---

[Abook](http://abook.sourceforge.net) is a simple Ncurses address book application. It's the perfect choice for those seeking something simple, both in terms of footprint and usability. Or perhaps you're just tired of keeping everything in the cloud. :)

And as a little bonus, it also works very well with the e-mail clients [Mutt](http://www.mutt.org) and [NeoMutt](https://neomutt.org), which I will cover further down in this blog post.

<div class="gallery">
	<a href="/../images/abook.png"><img src="/../images/thumbnails/abook.png"></a>
	<a href="/../images/abook_edit.png"><img src="/../images/thumbnails/abook_edit.png"></a>
</div>

# Installation

It's available in the official repositories as `abook` in both Arch Linux and Ubuntu. I assume it's the case for most other distros as well.

But if you want support for vCards (.csv) you need to patch and compile it yourself. If you choose to do so, jump to [their download page](http://abook.sourceforge.net/#download) and download the latest version.

Unpack the archive and `cd` to the directory:

```
$ tar -zxvf abook-0.6.0pre2.tar.gz
```
```
$ cd abook-0.6.0pre2
```

Download the patch:

```
$ wget http://abook.sourceforge.net/patches/abook_vcard_import.patch
```

I'm not sure if this is needed for all, but I needed [this patch](https://git.archlinux.org/svntogit/packages.git/tree/trunk/gcc5.patch?h=packages/abook) for it to successfully compile on Arch Linux:

```
$ wget -O gcc5.patch https://git.archlinux.org/svntogit/packages.git/plain/trunk/gcc5.patch?h=packages/abook
```
And now apply the patches:

```
$ patch -p1 -i abook_vcard_import.patch
```
```
$ patch -p1 -i abook_vcard_import.patch
```

Lastly compile and install it:

```
$ ./configure
```
```
$ make
```
```
# make install
```

# Configuration

I'm using Arch Linux (with the binary from their repositories) and the flag `--help` only mention this regarding a configuration: 

```
-C	--config	<file>		use an alternative configuration file
```

And the manual only mention this:

```
-C --config <filename>
	Use an alternative configuration file (default is $HOME/.abook/abookrc).
```

But I did some digging and found the file `/usr/share/man/man5/abookrc.5.gz`, which I extracted and found a manual with more information — including variables for the configuration. Why is this not the default manual? Anyway, I converted it to HTML and uploaded it [here](/../files/abook_manpage.html).

And this is what my configuration looks like:

```
# Automatically save database on exit
set autosave=true

# Show all email addresses in list
set show_all_emails=true

# frequently used values:
#    -1          disabled
#    phone       Home Phone
#    workphone   Work Phone
#    fax         Fax
#    mobile      Mobile Phone
#    nick        Nickname/Alias
#    url         URL

# Command used to start mutt
set mutt_command=mutt

# Command used to print
set print_command=lpr

# Command used to start the web browser
set www_command=w3m

# address style [eu|us|uk]
set address_style=eu

# use ASCII characters only
set use_ascii_only=false

# Prevent double entry
set add_email_prevent_duplicates=false

# field to be used with "sort by field" command
set sort_field=nick

# show cursor in main display
set show_cursor=false

set index_format=" {name:30} {mobile:20} {workphone:20} {email:35}"
```

# Usage

When you start `abook` you get a menu on top hinting you about the three most common shortcuts; `?` for help, `a` to add an entry and `r` to remove an entry. The help shows you as expected all available commands:

| ? |               help |
| q |               quit |
| Q |               quit without saving |
| P |               quit and output selected item(s) to stderr |
| ^L |              refresh screen |
|
| arrows / j,k |    scroll list |
| enter |           view/edit item |
| a |               add item |
| r / del |         remove selected items |
| M |               merge selected items (into top one) |
| D |               duplicate item |
| U |               remove duplicates |
|
| space |           select item |
| + |               select all |
| - |               unselect all |
| * |               invert selection |
|
| w |               write database to disk |
| l |               read database from disk |
| C |               clear whole database |
| i |               import database |

# Importing an address book

To import an adress book you use this syntax:

```
abook --convert --infile <file> --informat <input format> --outfile ~/.abook/addressbook --outformat <output format>
``````

As an example, let's import a vCart adress book:

```
abook --convert --infile example.csv --informat csv --outfile ~/.abook/addressbook --outformat abook
```

There's also support for importing these two kinds of address books:

* ldif (Netscape Address books)
* Pine aldress books

But I have no idea who uses those two?

# Exporting an address book

For some reason it supports exporting to more formats than it can import. The website lists these as compatible formats:

* ldif / Netscape address book (.4ld)
* Pine address book
* HTML
* mutt alias
* GnomeCard (VCard) address book
* elm alias
* plain text
* Spruce address book

Exporting an adress book works as expected in the same way as importing an adress book. In this example I'm exporting my adress book to a plain text file:

```
$ abook --convert --infile ~/.config/abook/addressbook --informat abook --outfile hund.text --outformat text
```
And the result is a plain text file looking like this:

```
-----------------------------------------
Hund's address book
-----------------------------------------


-----------------------------------------

Hund

hund@example.com

hund0b1.gitlab.io

-----------------------------------------

Katt

katt@example.com

Home Phone: 123 456 789

-----------------------------------------
```

# Using Abook with Mutt

As I previously mentioned, it's possible to use Abook with Mutt, which is partly the reason I chose to go with Abook a long time ago.

For Abook to work with Mutt you need to add this part to your Mutt configuration:

```
set query_command = "abook --mutt-query '%s'"
macro generic,index,pager \ca "<shell-escape>abook<return>" "launch abook"
macro index,pager A "<pipe-message>abook --add-email<return>" "add the sender address to abook"
```

Now when you're composing a new message you can either just `<tab>` to bring out a list with all your contacts. It's also possible to type some letters to filter out some results before tabbing.

And if you want to add a contact to Abook, you just use the key `A` when you're either reading the message or when it's selected in the mailbox.
