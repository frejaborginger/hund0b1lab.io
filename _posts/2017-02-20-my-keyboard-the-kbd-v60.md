---
layout: post
title: My keyboard — KBParadise V60
tags:
 - My_keyboards
 - Hund
---

This is my slightly modded KBParadise V60 with new keycaps and a custom cable. It's also my first and last ANSI keyboard.

[![](/images/keyboards/thumbnails/kbparadise_v60.jpg)](/images/keyboards/kbparadise_v60.jpg)

| Brand:  | KBParadise |
| Model:       | V60 |
| Size:   | 60% |
| Switches:      | Gateron Red |
| Keycaps:    | NPKC PBT blanks |
| Artisan:      | Idea23 [[Website](https://www.idea23.com)] |
| Cable:        | Custom, PexonPCs [[Website](https://pexonpcs.co.uk)] |
