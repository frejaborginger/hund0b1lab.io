---
layout: post
title: My keyboard — Let's Split
tags:
 - My_keyboards
 - Hund
---

This is the first keyboard that I have built myself. I wanted something easy and fairly inexpensive to start with since I didn't have any previous experience with soldering. I also wanted to try both a ortholinear keyboard and a split layout.

[![](/images/keyboards/thumbnails/lets_split.jpg)](/images/keyboards/lets_split.jpg)

I enjoyed building it and the soldering job turned out pretty good for being a self-taught first timer. And I really liked having a split keyboard, but the ortholinear layout is not for me. :)

| Size:      | 40% |
| Case:           | Let's Split, Smashing Acrylic [[Website](https://smashingacrylics.co.uk/product/lets-split-acrylic-cases/)] |
| PCB:        | Let's Split, Smashing Acrylic [[Website](https://smashingacrylics.co.uk/product/lets-split-pcb-kits/)] |
| Switches:         | Gateron Yellow |
| Keycaps:    | NPKC PBT blanks |


