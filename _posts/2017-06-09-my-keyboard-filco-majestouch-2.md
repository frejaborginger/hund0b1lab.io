---
layout: post
title: My keyboard — Filco Majestouch 2
tags:
 - My_keyboards
 - Hund
---

This keyboard is about a year old now, but I recently got my new custom and very expensive cable for it, so I thought it was time to share it here.

[![](/images/keyboards/thumbnails/filco_majestouch_2_tkl.jpg)](/images/keyboards/filco_majestouch_2_tkl.jpg)

It's also rocking a custom controller called [The Pegasus Hoof](http://bathroomepiphanies.com/controllers/), which makes it fully programmable via the open source [TMK firmware](https://github.com/tmk/tmk_keyboard). This adds a lot of neat features like layers and macros.

| Brand:  | Filco |
| Model:       | Majestouch 2 |
| Size:   | TKL / 80% |
| Switches:      | Cherry MX Red |
| Keycaps:    | NPKC PBT blanks |
| Cable:        | Custom, PexonPCs [[Website](https://pexonpcs.co.uk)] |
