---
layout: post
title: My keyboard - The Black Diamond
tags:
 - My_keyboards
 - Hund
---

This is my second custom built keyboard. I named it the Black Diamond since the case is called Diamond in Chinese, which sounds way better than the international name which is "5°".

[![](/images/keyboards/black_diamond-thumb.jpg)](/images/keyboards/black_diamond.jpg)

| Size:   | 60% |
| Case:       | [Diamond](https://kbdfans.myshopify.com/collections/case/products/pre-orderkbdfans-5-60-case), KBDfans |
| PCB:    | [S60-X](https://sentraq.com/collections/parts-1/products/60-rgb-underglow-pcb), Sentraq |
| Switches:      | [Tealios](https://zealpc.net/collections/switches/products/tealios), ZealPC |
| Springs:      | [SPRiT Edition (60g)](https://www.candykeys.com/product/60g-mx-springs-sprit-edition-100x/), SPRiT |
| Keycaps:    | [PBT Blanks](https://www.massdrop.com/buy/blank-pbt-keycaps?utm_source=linkshare&referer=CPRCE7), NPKC |
| Artisan:      | [Cherry Blank](https://skdcables.com/index.php/product/blanks/), SKD Cables |
