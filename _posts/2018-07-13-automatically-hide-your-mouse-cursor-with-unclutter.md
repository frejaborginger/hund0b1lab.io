---
layout: post
title: Automatically hide your mouse cursor with Unclutter
tags:
  - Unclutter
  - Mouse
---

If you almost never use the computer mouse like me, you are most likely also finding the mouse cursor distracting and in the way of your work. But there's actually a way of automatically hiding it when it's not being used and it's made possible with the tool [Unclutter](http://www.ibiblio.org/pub/X11/contrib/utilities/unclutter-8.README), or more preferably with [unclutter-xfixes](https://github.com/Airblader/unclutter-xfixes). 

Unclutter is actually a very old tool and [the latest version](https://www.ibiblio.org/pub/X11/contrib/utilities/) of it dates back to 1996, this is why the user [Airblader](https://github.com/Airblader) (the person behind [i3-gaps](https://github.com/Airblader/i3)) decided to fork the project under the name unclutter-xfixes and bring it up to date.

But with that said, I've been using the old Unclutter until recently and I have never had any issues with it. So it should work just fine, but unclutter-xfixes is the recommended choice.

# Usage

Using Unclutter/unclutter-xfixes is nothing more than running the command `unclutter`. And if you check out the manual, you will find a few optional flags to tinker with, like the flag `--timeout <n>` which changes the number of seconds after which the cursor should be hidden.

# Installation

## Unclutter

Unclutter is both available in the repositories for Arch Linux and Ubuntu as `unclutter`:

**Arch Linux:**

```
# pacman -S unclutter
```

**Ubuntu:**

```
# apt install unclutter
```

## unclutter-xfixes

### Arch Linux

unclutter-xfixes is available via the Arch User Repo as the package [unclutter-xfixes-git](https://github.com/Airblader/unclutter-xfixes).

### Ubuntu

If you're using Ubuntu (or most likely any other Linux distribution) you then need to compile it yourself.

Start with installing the dependencies:

```
# apt install build-essential libev-dev libx11-dev libxfixes-dev libxi-dev pkg-config
```

And optionally you also might want to install the package `asciidoc` if you want to include the manual. Do note that I needed to download more than 700MB worth of packages for that, which is why I only added it as a optional dependency. But it's only needed when compiling the package, so it's okay to uninstall it when you done compiling unclutter-xfixes.

And lastly, continue to download the source code and compile it:

```
$ git clone https://github.com/Airblader/unclutter-xfixes
$ cd unclutter-xfixes
$ make
# make install
```
