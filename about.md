---
layout: page
title: About
permalink: /about/
---

This website is intended to be a place for me to recommend good software and to document my adventures in Linux, the commandline, minimalism and sometimes my hobby for mechanical keyboards. And since English is not my first language I don't mind if you point out any mistakes I make with the grammar. :)

The website is powered by the blog-aware, static site generator [Jekyll](https://jekyllrb.com) and it's hosted at [GitLab](https://about.gitlab.com). It's free from JavaScript, cookies and anything that can track you. The source code for the complete website can be found in the public repository on my GitLab account [here](https://gitlab.com/Hund0b1/hund0b1.gitlab.io). It's not the prettiest code in the world, but it's getting improved on now and then.

And if you like the content of my website, consider adding the <a href="{{ site.baseurl }}/feed.xml">web feed</a> to your feed reader for future updates. :)

# $ whoami

Hi, my name is Hund! :)

I'm a Swedish [INFJ](https://www.16personalities.com/infj-personality) dork and an avid Linux user, I'm a big advocate of both the [UNIX philosophy](https://en.wikipedia.org/wiki/Unix_philosophy) and the [KISS principle](https://en.wikipedia.org/wiki/KISS_principle), which is two healthy philosophical approaches to minimalist and modular software. Something that also can be applied to most things in life and not just computer software.

I got into Linux back in late 2006 when a friend gave me [a printed CD with Ubuntu 6.06](/images/ubuntu_6.06_cd.jpg). It didn't take long before I was completely sold on Linux and the libre software philosophy. I quickly became very active in the Swedish Ubuntu LoCo and I ended up becoming an Ubuntu member in 2009.

I'm also a big fan of tiling window managers, the terminal, Vim, Vi-like and text based applications and the Solarized colour scheme.

[![](/../images/thumbnails/my_desktop_october_2017.png)](/../images/my_desktop_october_2017.png)

Minimalism is also something that's a big part of my life. The meaning of minimalism can vary a lot depending on who you ask, but for me it's about keeping a healthy balance in life, it's about freedom and thoughtful living. And not necessarily so much about limiting yourself to a maximum amounts of items or setting your desktop wallpaper to the color `#000000` and then call it a day.

# My Battlestation

[![My Battlestation](/images/thumbnails/my_battlestation_january_2018.jpg)](/images/my_battlestation_january_2018.jpg)
<div class="button"><a href="/battlestation.html">More information</a></div>

# Links

* [deviantART](https://hundone.deviantart.com) - I'm a long time user of deviantART. I'm not as active as I used to be, but I still upload the occasional desktop screenshot and a few other things from time to time though.
* [GitLab](https://gitlab.com/users/Hund0b1/projects) - I upload all my dotfiles here. If you like something from some of my desktop screenhots you will most likely find it here. I try to keep the configuration files fairly updated.
* [Mastodon](https://linuxrocks.online/@hund) - You can find me on the instance [LinuxRocks.Online](https://linuxrocks.online/@hund). It's the only social media I'm currently using.
* [Steam](https://steamcommunity.com/id/hund0b1) - This is my dirty little secret! I used to be a huge gamer -- with COD4 as my number one favourite game. But I haven't used Microsoft Windows in a very long time now and I'm hoping it's going to stay that way. I'm currently not playing any games besides Minecraft, but feel free to add me on Steam, who knows what happens in the future.
