---
layout: page
title: Länktips
permalink: /lanktips.html
---

<div class="lanktips-index" id="back">
    <h3>Innehåll</h3>
    <ul>
        <li><a href="#gnulinux">GNU/Linux</a></li>
        <li><a href="#graffiti">Graffiti</a></li>
        <li><a href="#mat">Mat, dryck & bak</a></li>
        <li><a href="#tangentbord">Tangentbord</a></li>
        <li><a href="#teknik">Teknik</a></li>
        <li><a href="#retro">Retro</a></li>
        <li><a href="#Spel">Spel</a></li>
        <li><a href="#underhallning">Underhållning</a></li>
        <li><a href="#vetenskap">Vetenskap & lärorikt</a></li>
    </ul>
</div>

<div style="margin: -7px 0 20px 0; color: #828282; font-size: 12px">Senast uppdaterad: 16 februari, 2018.</div>

För allas trevnad pekar alla YouTube-länkar till [Hooktube](https://hooktube.com). :)

# GNU/Linux
 
* YouTube
  * [BigDaddyLinux](https://www.hooktube.com/channel/UCtZRKfyvx7GUEi-Lr7f4Nxg)
  * [Chris Were](https://hooktube.com/channel/UCAPR27YUyxmgwm3Wc2WSHLw)
  * [Conner McDaniel](https://www.hooktube.com/channel/UCgREucssIfY9e0Iy3yhse8w)
  * [gotbletu](https://hooktube.com/channel/UCkf4VIqu3Acnfzuk3kRIFwA)
  * [GrayWolfTech](http://www.hooktube.com/channel/UC9LMOoh4KjD0FmHxhM_LGrQ)
  * [Level1Linux](https://hooktube.com/channel/UCOWcZ6Wicl-1N34H0zZe38w)
  * [Linux Spot](https://www.hooktube.com/channel/UCR4lODJVvXHiTY3wmWKv32Q)
  * [Luke Smith](https://www.hooktube.com/channel/UC2eYFnH61tmytImy1mTYvhA)

<h1 id="Graffiti">Graffiti</h1><a href class="back" id="back">Tillbaka &#8593;</a>

* YouTube
  * [DesanTwentyOne](https://hooktube.com/channel/UC-aKKzThE5oEZsuusB2mG9w)
  * [Ed-Mun PDF](https://hooktube.com/channel/UCD6pZMDrHptGCjRfIKB974w)
  * [Helio Bray](http://www.hooktube.com/channel/UCdxQMWUC681n4DK6yUP-MUA)
  * [Rasko](https://hooktube.com/channel/UC1ltKcWNeiKgQfrT_x-g0IA)

<h1 id="mat">Mat, dryck & bak</h1><a href class="back" id="back">Tillbaka &#8593;</a>

* YouTube
  * [Bingin With Babish](https://www.hooktube.com/channel/UCJHA_jMfCvEnv-3kRjTCQXw)
  * [Cocktail Chemistry](https://hooktube.com/channel/UC-o0CfpOyFJOfyWKtqS1hZQ)
  * [Man About Cake](https://www.hooktube.com/channel/UCcsBpRN1l9YsaLoElmJj2Jg)
  * [You Suck At Cooking](https://www.hooktube.com/channel/UCekQr9znsk2vWxBo3YiLq2w)

<h1 id="tangentbord">Tangentbord</h1><a href class="back" id="back">Tillbaka &#8593;</a>

* YouTube
  * [Chokkan - Mech in Japan](https://www.hooktube.com/channel/UCtLJJRsmtUgCWLj_Ud9URfA)
  * [Chyrosran22](http://www.hooktube.com/channel/UCD0y51PJfvkZNe3y3FR5riw)
  * [diykeyboards](https://www.hooktube.com/channel/UCgRZ8Izkne2x0EAkXEvPldg)
  * [Jan Lunge](https://www.hooktube.com/channel/UCi1Rgm2ulItUOagasB3B8sg)
  * [MechMerlin](http://www.hooktube.com/channel/UCdfrYMwAJ8LHvy8-j_WIxAw)
  * [Rhinofeed](http://www.hooktube.com/channel/UC5ClFK9Ko4ACRgXjRZc_GfA)
  * [skiwithpete](http://www.hooktube.com/channel/UCuwFJkahErfhbroefg12lzA)
  * [TaeKeyboards](http://www.hooktube.com/channel/UCllGwtW6scxAjM28fIgEozg)

<h1 id="teknik">Teknik</h1><a href class="back" id="back">Tillbaka &#8593;</a>

* YouTube
  * [Level1Techs](http://www.hooktube.com/channel/UC4w1YQAJMWOz4qtxinq55LQ)
  * [Linus Tech Tips](https://www.hooktube.com/channel/UCXuqSBlHAE6Xw-yeJA0Tunw)
  * [Strange Parts](https://www.hooktube.com/channel/UCO8DQrSp5yEP937qNqTooOw)
  * [SweClockers](https://hooktube.com/channel/UCW64r1wPzfj0W1qbzzfCgFg)
  * [Techquickie](https://hooktube.com/channel/UC0vBXGSyV14uvJ4hECDOl0Q)

<h1 id="retro">Retro</h1><a href class="back" id="back">Tillbaka &#8593;</a>

* YouTube
  * [Hjälte Björkdahl](http://www.hooktube.com/channel/UCRSrsZnj7jORBngSpkkXniQ)
  * [The 8-Bit Guy](https://hooktube.com/channel/UC8uT9cgJorJPWu7ITLGo9Ww)
  * [The Obsolete Geek](https://hooktube.com/channel/UChJpPIRfNNqlB0dwlQQVLVQL)

<h1 id="spel">Spel</h1><a href class="back" id="back">Tillbaka &#8593;</a>

* YouTube
  * [9 Lives](https://hooktube.com/channel/UC3NzAEBuUxcU_RF1cld1OMw)
  * [Direwolf20](https://hooktube.com/channel/UC_ViSsVg_3JUDyLS3E2Un5g)
  * [Etho](http://www.hooktube.com/channel/UCFKDEp9si4RmHFWJW1vYsMA)
  * [IJQI](https://hooktube.com/channel/UCeGhxVDUTH74_a91syJS6mg)
  * [ilmango](http://www.hooktube.com/channel/UCHSI8erNrN6hs3sUK6oONLA)
  * [Jansey](https://www.hooktube.com/channel/UCdeIRMdmfUKVBp3EujuTQ5w)
  * [LevelCapGaming](https://hooktube.com/channel/UClMXf2oP5UiW_V4dwHxY0Mg)
  * [Madnes64](https://hooktube.com/channel/UCnj7FzYS_fuFZV5DaBtrMxA)

<h1 id="underhallning">Underhållning</h1><a href class="back" id="back">Tillbaka &#8593;</a>

* YouTube
  * [DONG](https://hooktube.com/channel/UClq42foiSgl7sSpLupnugGA)
  * [Mark Rober](https://www.hooktube.com/channel/UCY1kMZp36IQSyNx_9h4mpCg)
  * [People are Awesome](https://hooktube.com/channel/UCIJ0lLcABPdYGp7pRMGccAQ)
  * [Stop a Douchebag](https://hooktube.com/channel/UCMrKscEv_Ri1pvlRsLxsqJQ)
  * [Stop a Douchebag World](https://hooktube.com/channel/UCQMs9pijXYAdqvkEMJyCM4g)
  * [Vsauce](https://hooktube.com/channel/UC6nSFpj9HTCZ5t-N3Rm3-HA)
  * [Vsauce2](https://hooktube.com/channel/UCqmugCqELzhIMNYnsjScXXw)
  * [Vsauce3](https://hooktube.com/channel/UCwmFOfFuvRPI112vR5DNnrA)

<h1 id="vetenskap">Vetenskap & underhållning</h1><a href class="back" id="back">Tillbaka &#8593;</a>

* YouTube
  * [AsapSCIENCE](https://hooktube.com/channel/UCC552Sd-3nyi_tk2BudLUzA)
  * [EngineerGuy](https://hooktube.com/channel/UC2bkHVIDjXS7sgrgjFtzOXQ)
  * [Kurzgesagt](https://hooktube.com/channel/UCsXVk37bltHxD1rDPwtNM8Q)
  * [MinuteEarth](https://hooktube.com/channel/UCeiYXex_fwgYDonaTcSIk6w)
  * [Primitive Technology](https://hooktube.com/channel/UCAL3JXZSzSm8AlZyD3nQdBA)
  * [Reactions](https://hooktube.com/channel/UCdJ9oJ2GUF8Vmb-G63ldGWg)
